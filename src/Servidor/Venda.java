package Servidor;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Vector;

public class Venda extends Thread{

    protected String cliente;    //ID/NOME CLIENTE
    protected InetAddress host; //ENDEREÇO MAQUINA DO CLIENTE
    protected ArrayList filmesComprados;
    
    
    public Venda(){
        filmesComprados = new ArrayList();
        // Inicia todos os filmes da venda como não vendidos
        filmesComprados.add(0); 
        filmesComprados.add(0);
        filmesComprados.add(0);
        filmesComprados.add(0);
        filmesComprados.add(0);
        filmesComprados.add(0);
        filmesComprados.add(0);
        filmesComprados.add(0);
        filmesComprados.add(0);
        filmesComprados.add(0);
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public ArrayList getFilmesComprados() {
        return filmesComprados;
    }

    public void setFilmeComprado(int posFilme) {
        this.filmesComprados.set(posFilme, 1);
    }

    public InetAddress getHost() {
        return host;
    }

    public void setHost(InetAddress host) {
        this.host = host;
    }  
}
