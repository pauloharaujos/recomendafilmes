package Servidor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Servidor {

    public static void main(String[] args) {    //PRINCIPAL... SERVIDOR
        new Servidor().executa();           
    }
    
    private int recomendacoes[][] = new int[10][11];
    private ArrayList<Venda> listaVendas = new ArrayList<>();  //LISTA VENDAS... COMPRA DOS CLIENTES
    private String filmes = "xxx;"
                            + "Velozes_e_Furiosos;"
                            + "Kung_Fu_Panda;"
                            + "Era_do_Gelo;"
                            + "Need_for_Speed;"
                            + "Renascida_do_Inferno;"
                            + "O_Chamado;"
                            + "18+;"
                            + "Doutor_Estranho;"
                            + "Capitao_America";

//----------------METODOS----------------------------------------------------------------------------------------------------------------    
    private String interpretaMensagem(String mensagem, InetAddress aHost) throws IOException { //METODO PARA INTERPRETAR MENSAGEM RECEBIDA
        mensagem = mensagem.trim();
        int cont = 0;// VERIFICA SE EXISTEM USUARIOS REPETIDOS
        String resposta = "";   //RESPOSTA QUE O METODO IRA RETORNAR
        String s[] = mensagem.split(";");   //DIVIDE STRING PELOS ;
 
        if (s[0].equals("1")) {   //ADICIONAR/CADASTRAR
            if (!this.listaVendas.isEmpty()) {    //SE LISTA NÃO VAZIA
                System.out.println("lista não vazia");
                for (int i = 0; i < this.listaVendas.size(); i++) { //PERCORRE A LISTA
                    if (this.listaVendas.get(i).cliente.equals(s[1])) {    //VERIFICA SE O USARIO É IGUAL AO DA LISTA

                        resposta = "0;erro;usuario_ja_cadastrado";
                        System.out.println("0;erro;usuario_ja_cadastrado");
                        cont++; //VERIFICA SE EXISTE USUARIO REPETIDOS
                    }
                }
                //SE NÃO EXISTIR USUARIO IGUAL... ADICIONA
                if (cont == 0) {
                    Venda v = new Venda();                    
                    v.setCliente(s[1]);
                    v.setHost(aHost);
                    this.listaVendas.add(v);

                    resposta = "0;sucesso;usuario_cadastrado";
                    System.out.println("0;sucesso;usuario_cadastrado");
                }
            } else {    //SE A LISTA ESTIVER VAZIA
                System.out.println("lista vazia");
                Venda v = new Venda();
                v.setCliente(s[1]);
                v.setHost(aHost);
                this.listaVendas.add(v);

                resposta = "0;sucesso;usuario_cadastrado";
                System.out.println("0;sucesso;usuario_cadastrado");
            }
        } else if (s[0].equals("2")) { //LISTAR FILMES DISPONIVEIS
            resposta = "2;" + this.filmes;          
            System.out.println("2;lista_filmes");
        } else if (s[0].equals("5")) { //Registra venda
           for (int i = 0; i < this.listaVendas.size(); i++) { //PERCORRE A LISTA
                if (this.listaVendas.get(i).getCliente().equals(s[1])) {    //VERIFICA SE O USARIO É IGUAL AO DA LISTA
                    listaVendas.get(i).setCliente(s[1]);                    
                    listaVendas.get(i).setHost(aHost);
                   
                    for(int x=2; x < s.length;x++){
                        if(s[x].equals("xxx")){
                            listaVendas.get(i).setFilmeComprado(0);
                        }else if(s[x].equals("Velozes_e_Furiosos")){
                            listaVendas.get(i).setFilmeComprado(1);
                        }else if(s[x].equals("Kung_Fu_Panda")){
                            listaVendas.get(i).setFilmeComprado(2);
                        }else if(s[x].equals("Era_do_Gelo")){
                            listaVendas.get(i).setFilmeComprado(3);
                        }else if(s[x].equals("Need_for_Speed")){
                            listaVendas.get(i).setFilmeComprado(4);
                        }else if(s[x].equals("Renascida_do_Inferno")){
                            listaVendas.get(i).setFilmeComprado(5);
                        }else if(s[x].equals("O_Chamado")){
                            listaVendas.get(i).setFilmeComprado(6);
                        }else if(s[x].equals("18+")){
                            listaVendas.get(i).setFilmeComprado(7);
                        }else if(s[x].equals("Doutor_Estranho")){
                            listaVendas.get(i).setFilmeComprado(8);
                        }else if(s[x].equals("Capitao_America")){
                            listaVendas.get(i).setFilmeComprado(9);
                        }                       
                    } 
                    
                    resposta = "5;" + "Filmes comprados com SUCESSO !";
                    String recomendados = buscaRecomendacoes(recomendacoes,listaVendas.get(i).getFilmesComprados());
                    resposta = resposta + ";" + recomendados;                    
                    geraNovasRecomendacoes(listaVendas);
                    i = listaVendas.size();
                }             
            }
           salvaLogVendas(this.listaVendas);         
        }
        return resposta;        
    }

//----------------METODOS----------------------------------------------------------------------------------------------------------------    
    public void executa() {
           
        String resposta;    //RECEBE A RESPOSTA DO INTERPRETADOR
        DatagramSocket aSocket = null;  //SOQUETE PARA ENVIO E RECEBIMENTO DE MENSAGENS

        try {
            carregaLogVendas(this.listaVendas);
            leRegrasRecomendacoes(); //Le as regras de recomendações do Servidor
            aSocket = new DatagramSocket(12345); //SOQUETE RECEBE A PORTA 12345

            System.out.println("------------------------------------------");
            System.out.println("SERVIDOR ONLINE");
            System.out.println("------------------------------------------");

            byte[] buffer = new byte[150];

            while (true) {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);//PACOTE PARA RECEBER A MENSAGEM
                aSocket.receive(request);   //RECEBE A MENSAGEM

                System.out.println("O cliente " + request.getAddress().getHostName() + " esta enviando: ");//AVISA QUE O CLIENTE ESTA MANDANDO MENSAGEM
                System.out.println(new String(request.getData()));    //IMPRIME MENSAGEM ENVIADA PELO CLIENTE

                resposta = this.interpretaMensagem(new String(request.getData()), request.getAddress());  //METODO PARA INTEPRETAR MENSAGEM
                byte[] m = resposta.getBytes();

                DatagramPacket reply = new DatagramPacket(m, resposta.length(), request.getAddress(), request.getPort()); //PACOTE PARA ENVIAR A MENSAGEM PARA O CLIENTE
                aSocket.send(reply);    //MENSAGEM É ENVIADA PARA O CLIENTE
            }
        } catch (SecurityException e) {
            System.out.println("Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Input Output: " + e.getMessage());
        } finally {
            if (aSocket != null) {
                aSocket.close();
            }
        }
    }
    
    public static void carregaLogVendas(ArrayList listaVendas){ //Le do Arquivo de vendas
        //Le do arquivo e preenche a lista de vendas 

        InputStream is = null;
        try {
            is = new FileInputStream("logVendas.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

     InputStreamReader isr = new InputStreamReader(is);
     BufferedReader br = new BufferedReader(isr);
 
     String s = null;
        try {
            s = br.readLine(); // primeira linha
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
     
     while (s != null) {
         System.out.println(s);
         try {
             
             if(s != null ){       
                String[] linha = s.split(" ");
                Venda v = new Venda();
                v.setCliente(linha[0]);  
                
                for(int z = 1; z < linha.length; z++){
                    int p = Integer.parseInt(linha[z]);
                    if(p == 1)
                      v.setFilmeComprado(z-1);
                }           
               
                listaVendas.add(v);             
                }
             s = br.readLine();
         } catch (IOException ex) {
             Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
         }
     }     
        try {   
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
    public static void salvaLogVendas(ArrayList listaVendas) throws IOException{
      
    Venda aux = new Venda();
    
    BufferedWriter buffer = new BufferedWriter(new FileWriter ("logVendas.txt"));

    for(int i = 0; i < listaVendas.size(); i++){           
        aux = (Venda) listaVendas.get(i);      
        buffer.write(aux.getCliente() + " " + aux.getFilmesComprados().get(0) + " " + aux.getFilmesComprados().get(1)
        + " " + aux.getFilmesComprados().get(2) + " " + aux.getFilmesComprados().get(3) + " " + aux.getFilmesComprados().get(4) + " " + aux.getFilmesComprados().get(5)
        + " " + aux.getFilmesComprados().get(6) + " " + aux.getFilmesComprados().get(7) + " " + aux.getFilmesComprados().get(8) + " " + aux.getFilmesComprados().get(9));       
        buffer.newLine();
       }
    buffer.close();
     
    
    } 
    public String buscaRecomendacoes(int matriz[][], ArrayList comprados){
        
       String[] f = this.filmes.split(";");
       String filmesRecomendados = "";
       ArrayList aux = new ArrayList();
       int l = 0, c =0;
       
       aux.add(0);aux.add(0);aux.add(0);aux.add(0);aux.add(0);aux.add(0);aux.add(0);aux.add(0);aux.add(0);aux.add(0);
       
       for(l = 0; l < 10; l++){
           if(comprados.get(l).equals(1)){ //Se o cliente comprou o filme da linha da matriz, entao verificar recomendacoes
                for(c = 1; c < 11; c++){
                    aux.set(c-1, matriz[l][c]); // Grava no vetor auxiliar as recomendacoes para esse filme
                }
           }
       }               
       //Retira a recomendação dos filmes que o cliente ja comprou
       for(l = 0; l< comprados.size(); l++){
           if(comprados.get(l).equals(1))
               aux.set(l, 0);
       }
       
       for(l = 0; l < aux.size(); l++){ 
           if(aux.get(l).equals(1)){
               filmesRecomendados = filmesRecomendados + ";" + f[l];             
           }
        } 
       return filmesRecomendados;
    }
    
    private void leRegrasRecomendacoes(){
         //Le do arquivo e preenche a lista de vendas 
       
        int l = 0;
        String[] f = this.filmes.split(";");
        
        InputStream is = null;
        try {
            is = new FileInputStream("recomendacoes.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

     InputStreamReader isr = new InputStreamReader(is);
     BufferedReader br = new BufferedReader(isr);
 
     String s = null;
        try {
            s = br.readLine(); // primeira linha
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
     
     while (s != null) {
         System.out.println(s);
         try {
             
             if(s != null ){       
                String[] linha = s.split(" "); 
                //Le as regras de recomendacoes do arquivo e preenche a matriz regras de recomendacoes
                    for(int c=0; c < 11; c ++){
                        recomendacoes[l][c] = Integer.parseInt(linha[c]);
                    }                    
                    l++;
                }
             s = br.readLine();
         } catch (IOException ex) {
             Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
         }
     }     
        try {   
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }    
    
    private void geraNovasRecomendacoes(ArrayList<Venda> listaVendas) throws IOException {
           
        int x = 0, y = 0;
        int xx = 0, xy=0;
        float fConf =0, fSup = 0; //Nivel de Suporte e Confianca
        int totalRegistros = listaVendas.size();
        
        while(x < 10){
            while(y < 10){
                for(int i = 0; i < totalRegistros; i++){
                    ArrayList aux = new ArrayList();

                    for(int k=0; k < 10; k++){
                        int ax = (int) listaVendas.get(i).getFilmesComprados().get(k);
                        aux.add(ax);
                    }
                    if(aux.get(x).equals(1)){ //Encontrou uma venda x e y
                        xx++; //Quantidade de registros X encontrados
                        if(aux.get(y).equals(1)){
                            xy++;
                        }
                    }
                }
                //Aqui ele tem que calcular a recomendacao X/Y. tipo filme 1 com o 2.
                if(xx != 0){
                    fSup = xy * 100 / totalRegistros;
                    fConf = xy * 100 / xx; 
                }
                if(fSup > 30 && fConf > 70)
                    recomendacoes[x][y+1] = 1;
                else
                    recomendacoes[x][y+1] = 0;
                y++;
                
                //zera as variaveis para buscar nova recomendacao.
                fSup = 0;
                fConf = 0;
                xx = 0;
                xy = 0;
            }
            //lembrar de zerar as variaveis ...
            x++;
            y = 0;
        }
        salvarRecomendacoes();
        
    }  
    
    public void salvarRecomendacoes() throws IOException{
      
    Venda aux = new Venda();
    
    BufferedWriter buffer = new BufferedWriter(new FileWriter ("recomendacoes.txt"));

    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 11;j++){        
          buffer.write(recomendacoes[i][j] + " ");
        }               
        buffer.newLine();
       }
    buffer.close();      
    } 
    
//    private void geraNovasRecomendacoes(ArrayList<Venda> listaVendas, ArrayList s) {
//           
//        String[] f = this.filmes.split(";");
//        ArrayList recomendados = new ArrayList(); //Armazena os filmes que podem ser recomendados                
//        int recomenda = 0;
//        
//        recomendados.add(0); recomendados.add(0); recomendados.add(0); recomendados.add(0);
//        recomendados.add(0); recomendados.add(0); recomendados.add(0); recomendados.add(0);
//        recomendados.add(0); recomendados.add(0);
//            
//        
//        for(int posFilme = 0; posFilme < s.size(); posFilme++){ //Verifica quais filmes foram vendidos na venda atual
//            if(s.get(posFilme).equals(1)){ //Se esse filme foi comprado, verificar quem mais comprou ele
//                for(int i = 0; i < listaVendas.size();i++){ //Percorre todas as venda e ve quem mais comprou esse filme
//                                     
//                    ArrayList aux = new ArrayList();
//                   
//                    for(int k=0; k < 10; k++){
//                        int ax = (int) listaVendas.get(i).getFilmesComprados().get(k);
//                        aux.add(ax);
//                    }
//                    if(aux.get(posFilme).equals(1)){ //Encontrou outra venda que tambem comprou esse filme
//                        
//                        for(int h=0; h < posFilme; h++){
//                            recomenda  = (int) recomendados.get(h);                    
//                            recomenda =  recomenda + (int) aux.get(h);
//                            recomendados.set(h, recomenda); 
//                        }
//                        
//                        for(int r = posFilme+1; r < 9; r++){ // percorre os filmes da venda que encontrou semelhante e grava os possiveis filmes para recomendacao
//                            recomenda  = (int) recomendados.get(r);                    
//                            recomenda =  recomenda + (int) aux.get(r);
//                            recomendados.set(r, recomenda); 
//                        }   
//                    }
//                }
//                //Calcular e jogar na matriz, quem comprou o filme posFilme tbm comprou...
//                
//                //Retira a recomendação dos filmes que o cliente ja comprou
//                for(int ky = 0; ky < s.size(); ky++){
//                    if(s.get(ky).equals(1))
//                        recomendados.set(ky, 0);
//                }
//                calcularConfianca(recomendados,f, listaVendas); //Calcula a confinca 30%
//                System.out.println("Recomendados: " + recomendados.toString());
//            }
//        }    
//    }    

//    private String buscaRecomendacoes(ArrayList<Venda> listaVendas, ArrayList s) {
//           
//        String[] f = this.filmes.split(";");
//        ArrayList recomendados = new ArrayList(); //Armazena os filmes que podem ser recomendados                
//        int recomenda = 0;
//        
//        recomendados.add(0); recomendados.add(0); recomendados.add(0); recomendados.add(0);
//        recomendados.add(0); recomendados.add(0); recomendados.add(0); recomendados.add(0);
//        recomendados.add(0); recomendados.add(0);
//        
//        Venda venda = new Venda();
//        
//        for(int posFilme = 0; posFilme < s.size(); posFilme++){ //Verifica quais filmes foram vendidos na venda atual
//            if(s.get(posFilme).equals(1)){ //Se esse filme foi comprado, verificar quem mais comprou ele
//                for(int i = 0; i < listaVendas.size();i++){ //Percorre todas as venda e ve quem mais comprou esse filme
//                                     
//                    ArrayList aux = new ArrayList();
//                   
//                    for(int k=0; k < 10; k++){
//                        int ax = (int) listaVendas.get(i).getFilmesComprados().get(k);
//                        aux.add(ax);
//                    }
//                    if(aux.get(posFilme).equals(1)){ //Encontrou outra venda que tambem comprou esse filme
//                        
//                        for(int h=0; h < posFilme; h ++){
//                            recomenda  = (int) recomendados.get(h);                    
//                            recomenda =  recomenda + (int) aux.get(h);
//                            recomendados.set(h, recomenda); 
//                        }
//                        
//                        for(int r = posFilme+1; r < 9; r++){ // percorre os filmes da venda que encontrou semelhante e grava os possiveis filmes para recomendacao
//                            recomenda  = (int) recomendados.get(r);                    
//                            recomenda =  recomenda + (int) aux.get(r);
//                            recomendados.set(r, recomenda); 
//                        }   
//                    }
//                }
//            }
//        }
//       System.out.println("Recomendados: " + recomendados.toString());
//        
//       String aux = "";
//       
//       //Retira a recomendação dos filmes que o cliente ja comprou
//       for(int ky = 0; ky < s.size(); ky++){
//           if(s.get(ky).equals(1))
//               recomendados.set(ky, 0);
//       }
//       
//       for(int y = 0; y < recomendados.size(); y++){                    
//           int confianca = (int)recomendados.get(y)*100/listaVendas.size(); // Calcula o nivel de aparecimetos desse padrao
//                 
//           if(confianca >= 30){
//               aux = aux + ";" + f[y];             
//           }
//        }        
//        return aux;
//    }    
}
