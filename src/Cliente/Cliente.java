package Cliente;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Cliente {

    private String nomeDNS = "localhost";

    public String executa(String mensagem) {

        String resposta = "";
        DatagramSocket aSocket = null;

        try {
            aSocket = new DatagramSocket(); //RESPONSAVEL POR FAZER O ENVIO DOS PACOTES
            byte[] m = mensagem.getBytes(); //RECEBE MENSAGEM CONVERTIDA EM BYTES

            InetAddress aHost = InetAddress.getByName(nomeDNS); //PEGA NOME OU ENDEREÇO IP

            int serverPort = 12345;  //PORTA DO SERVIDOR
            DatagramPacket request = new DatagramPacket(m, mensagem.length(), aHost, serverPort);   //PACOTE PARA ENVIAR MENSAGEM
            aSocket.send(request);  //CLIENTE MANDA A MENNSAGEM PARA O SERVIDOR

            byte[] buffer = new byte[150];
            DatagramPacket reply = new DatagramPacket(buffer, buffer.length);   //PACOTE PARA RECEBER MENSAGEM
            aSocket.receive(reply); //CLIENTE RECEBE A MENSAGEM

            System.out.println("Servidor devolve: " + new String(reply.getData()));

            resposta = new String(reply.getData());

        } catch (SecurityException e) {
            System.out.println("Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Input Output: " + e.getMessage());
        } finally {
            if (aSocket != null) {
                aSocket.close();
            }
        }
        return resposta;
    }
}
