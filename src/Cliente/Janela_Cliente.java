package Cliente;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class Janela_Cliente extends javax.swing.JFrame {

    private int operacao;
    private Cliente cliente;

    public Janela_Cliente() {   //CONSTRUTOR
        initComponents();
        this.cliente = new Cliente();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel_Nome = new javax.swing.JLabel();
        jTextField_Nome = new javax.swing.JTextField();
        jButton_Cadastrar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList_Filmes = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList_Recomendados = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList_Comprados = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton_Comprar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cliente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel_Nome.setText("Nome:");

        jButton_Cadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/login.png"))); // NOI18N
        jButton_Cadastrar.setText("Cadastrar");
        jButton_Cadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_CadastrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel_Nome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField_Nome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton_Cadastrar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel_Nome)
                .addComponent(jTextField_Nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jButton_Cadastrar))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filmes / Compras / Recomendações", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jList_Filmes.setEnabled(false);
        jScrollPane1.setViewportView(jList_Filmes);

        jList_Recomendados.setEnabled(false);
        jScrollPane2.setViewportView(jList_Recomendados);

        jList_Comprados.setEnabled(false);
        jScrollPane3.setViewportView(jList_Comprados);

        jLabel2.setText("Filmes");

        jLabel3.setText("Recomendados");

        jLabel4.setText("Comprados");

        jButton_Comprar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/cart.png"))); // NOI18N
        jButton_Comprar.setText("Finalizar compra");
        jButton_Comprar.setEnabled(false);
        jButton_Comprar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_ComprarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jLabel3)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(0, 95, Short.MAX_VALUE)
                                .addComponent(jButton_Comprar))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_Comprar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jScrollPane1, jScrollPane3});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

//=================PRINCIPAL==============================================================================================================
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Janela_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Janela_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Janela_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Janela_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Janela_Cliente().setVisible(true);
            }
        });
    }

//=============BOTÕES==================================================================================================================
    private void jButton_CadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_CadastrarActionPerformed
        // TODO add your handling code here:
        operacao = 1;   //OPERAÇÃO A SER REALIZADA
        String usuario = jTextField_Nome.getText(); //PEGA ID DO USUARIO

        if (validarCadastro(usuario) == true) { //VERIFICA CAMPOS PREENCHIDOS
            String mensagem = (operacao + ";" + usuario);   //MENSAGEM FORMADA PELA OPERAÇÃO + ID DO CLIENTE        
            this.interpretaMensagem(cliente.executa(mensagem));//PASSA MENSAGEM PARA CLIENTE... RECEBE DE VOLTA UMA RESPOSTA
        }
    }//GEN-LAST:event_jButton_CadastrarActionPerformed
//=============BOTÕES==================================================================================================================
    private void jButton_ComprarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ComprarActionPerformed
        // ODO add your handling code here:
        DefaultListModel model = new DefaultListModel();
        Object[] lista = jList_Filmes.getSelectedValuesList().toArray(); 
        String operacao = "5";
        String mensagem = null;
        mensagem = operacao + ";" + jTextField_Nome.getText();
             
        for(int j =0; j < lista.length;j++){ 
            model.addElement(lista[j]);
            mensagem = mensagem + ";" + lista[j];
        } 
        this.jList_Comprados.setModel(model);          
        mensagem = mensagem.trim();
        this.interpretaMensagem(cliente.executa(mensagem));//PASSA MENSAGEM PARA CLIENTE... RECEBE DE VOLTA UMA RESPOSTA
       
    }//GEN-LAST:event_jButton_ComprarActionPerformed

//==================METODOS=============================================================================================================    
    private void interpretaMensagem(String mensagem) {  //INTERPRETA MENSAGEM RECEBIDA DO SERVIDOR

        String[] s = mensagem.split(";");   //DIVIDE A STRING PELOS ;

        //s[0] = OPERAÇÃO
        //s[1] = USUARIO
        //...
        if (s[0].equals("0")) { //RESPOSTA ADICIONAR
            if (s[1].equals("sucesso")) {   //EXIBE MENSAGEM E LIBERA COMPONENTES DE TELA
                JOptionPane.showMessageDialog(this, mensagem);

                //LIBERA CAMPOS... PARA REALIZAR COMPRA
                jList_Filmes.setEnabled(true);
                jList_Comprados.setEnabled(true);
                jList_Recomendados.setEnabled(true);
                jButton_Comprar.setEnabled(true);

                //BLOQUEIA CAMPOS... PARTICIPAR(CADASTRO)
                jButton_Cadastrar.setEnabled(false);

                this.interpretaMensagem(cliente.executa("2"));   //SOLICITA A LISTA DE FILMES 

            } else if (s[1].equals("erro")) {   //APENAS EXIBE MENSAGEM
                JOptionPane.showMessageDialog(this, mensagem);
            }
        } else if (s[0].equals("2")) {     //PREENCHER LISTA DE FILMES
            String aux = s[1];
            for(int k = 2;k < s.length;k++){
                aux = aux + ";" + s[k];
            }            
            String[] aux1 = aux.split(";");
            
            jList_Filmes.setModel(new DefaultComboBoxModel<>(aux1));
        }else if (s[0].equals("5")) {
            JOptionPane.showMessageDialog(this, s[1]);      
            
            String aux = s[2];
            for(int k = 3;k < s.length;k++){
                aux = aux + ";" + s[k];
            }            
            String[] aux1 = aux.split(";");
            
            jList_Recomendados.setModel(new DefaultComboBoxModel<>(aux1));
          
        }
    }

//==================METODOS=============================================================================================================
    private boolean validarCadastro(String s) {

        String erro = "";

        jLabel_Nome.setForeground(Color.black);
        if (s.equals("")) {
            erro = "!!! NOME INVALIDO !!!\n";
            jLabel_Nome.setForeground(Color.red);
        }

        if (erro.equals("")) {
            return true;
        } else {
            JOptionPane.showMessageDialog(this, erro, "ERRO PARTICIPAR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_Cadastrar;
    private javax.swing.JButton jButton_Comprar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel_Nome;
    private javax.swing.JList<String> jList_Comprados;
    private javax.swing.JList<String> jList_Filmes;
    private javax.swing.JList<String> jList_Recomendados;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField_Nome;
    // End of variables declaration//GEN-END:variables
}
